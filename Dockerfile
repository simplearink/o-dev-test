FROM python:3.6-slim-stretch

WORKDIR /app

COPY requirements.txt .

RUN pip3 install -r requirements.txt

RUN python3 -m spacy download en_core_web_sm

COPY . .