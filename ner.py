import spacy
import argparse as ap

parser = ap.ArgumentParser()
parser.add_argument('--text', type=str, default="")

args = parser.parse_args()

text_to_analyze = args.text

nlp = spacy.load("en_core_web_sm")

if text_to_analyze:
    text = nlp(text_to_analyze)

    ners_from_text = []

    for ent in text.ents:
        if ent.label_ == "PERSON":
            ners_from_text.append(ent.text)

    print(ners_from_text)
else:
    print("No text was found, try again")