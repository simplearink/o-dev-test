## O. Dev Test Task Data Science

### Done by Arina Kuznetsova

### How to run the project?

Pull the content of this repo and type the following:

`TEXT="<your text>" docker-compose up`

or

`sudo TEXT="<your text>" docker-compose up`

depending on your permissions.

TEXT is environment variable which contains input text.

### Examples

**I**: 'Arina Kuznetsova adores Mikhail Tkachenko and his awesome beagle Barsa' \
**O**: ['Arina Kuznetsova', 'Mikhail Tkachenko']

**I**: 'Vladimir Putin and Angela Merkel are good friends' \
**O**: ['Vladimir Putin', 'Angela Merkel']
